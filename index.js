// console.log ("Hello World");

// [Section] Arithmetic Operators
let x = 1397;
console.log("The value of x is " + x);
let y = 7831;
console.log("The value of y is " + y);

// Addition Operator
let sum = x + y;
console.log("Result of Addition Operator: " + sum);

// Difference Operator
let difference = x - y;
console.log("Result of Subtraction Operator: " + difference);

// Multipicaltion Operator
let product = x * y;
console.log("Result of Multiplication Operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of Division Operator: " + quotient);

// Modulo
let remainder = y % x;
console.log("Result of Modulo: " + remainder);

let secondRemainder = x % y;
console.log("Result of Modulo: " + secondRemainder);



// [Section] Assignment Operators

// Basic Assignment Operator (=) - adds the value of the right operand to a variable and assigns the result of the value.
let assignmentNumber = 8;
console.log(assignmentNumber);

// Addition Assignment Operator (+=) - adds the value of the right operand to a variable and assigns the results to the variable.

/* (long method)
assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);*/

assignmentNumber += 2;
console.log("Result of Addition Assignment Operator: " + assignmentNumber);

// Subtraction Assignment Operator (-=)
assignmentNumber -= 2;
console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);

// Multiplication Assignment Operator (*=)
assignmentNumber *= 4;
console.log("Result of Multiplication Assignment Operator: " + assignmentNumber);

// Division ASsignment Operator (/=)
assignmentNumber /= 8;
console.log("Result of Division Assignment Operator: " + assignmentNumber);



// [Section] Multiple Operators and Parentheses
/*
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 /5;
console.log("Result of MDAS Rule: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of PEMDAS Rule: " + pemdas);


// [Section] Incrementation and Decrement
// Operators that add or subtract value by 1 and reassigning the value of the variable where the increment and decrement was applies to 

let z = 1 

// Pre-Increment (+1 in front) 
let increment = ++z;
console.log("Result of z in pre-increment: " + z);
console.log("Result of increment in pre-increment: " + increment);

// Post-Increment (+1 at the end)
increment = z++;
console.log("Result of z in post-increment: " + z);
console.log("Result of increment in post-increment: " + increment);
increment = z++;
console.log("Result of increment in post-increment: " + increment);

let p = 0;

// Pre-Decrement (-1 in front)
let decrement = --p;
console.log("Result of p in pre-decrement: " + p);
console.log("Result of decrement in pre-decrement: " + decrement);

// Post-Decrement (-1 at the end)
decrement = p--;
console.log("Result of p in post-decrement: " + p);
console.log("Result of decrement in post-decrement: " + decrement);


// [Section] Type Coercion
/*
	Type coercion - automatic of implicit conversion of values from one data type to another.
		- This happens when opperations are performed on different data types that would normally not be possible and yield irregular results
		- Values are automatically converted from one data type to another in order to resolve operations
*/

// Example of Coercion
let numA = "10";
let numB = 12;

// Adding or concatinating a string and a number will result into string. This can be proven in the console by looking at the color of the displayed text.
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

// Example of Non-Coercion
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Example of Type Coercion: Boolean Value and Number
// Value of true = 1
// Value of false = 0
let numE = true + 1;
console.log(numE);


// [Section] Comparison Operators
let juan = 'juan';

	// Equality Operator (==)
	// It checks whether the operands are equal or have the same value
	let isEqual = 1 == 1;
	console.log(typeof isEqual);

	console.log (1 == 2);
	console.log (1 == '1');

	// Strict Equality Operator (===)
	console.log (1 === '1');
	console.log ('juan' == 'juan');
	console.log ('juan' == juan);

	// Inequality (!=)
	// Checks whether the operands are not equal or have different content. It attempts to convert and compare oprands of different data types.
	console.log (1 != 1);
	// ans: false
	console.log (1 != 2);
	// ans: true
	console.log (1 != '1');
	// ans: false

	// Strict Inequality Operator
	console.log (1 !== '1');
	// ans: true


// [Section] Relational Operators
// Some comparison operators check whether one value is greater or less than to the other value.
	
	let a = 50;
	let b = 65;

	// GT or Greater Than Operator (>) ans: false
	let isGreaterThan = a > b;
	console.log (isGreaterThan);

	// LT or Less Than Operator (<) ans: true
	let isLessThan = a < b;
	console.log (isLessThan);

	// GTE or Greater Than or Equal Operator (>=) ans: false
	let isGTOrEqual = a >= b;
	console.log (isGTOrEqual);

	// LTE or Less Than or Equal Operator (<=) ans: true
	let isLTOrEqual = a <= b;
	console.log (isLTOrEqual);

	let numStr = "30";
	console.log (a > numStr);
	// ans: true

	console.log (b <= numStr);
	// ans: false


// [Section] Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical And Operators (&& - double ampersand)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered
	console.log ("Result of Logical And Operator: " + allRequirementsMet);

	// Logical Or Operator (|| - double pipe)
	// Returns true if one of the operand is true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log ("Result of Logical Or Operator: " + someRequirementsMet);

	// Logical Not Operator (! = exclamation point)
	let someRequirementsNotMet = !isRegistered;
	console.log ("Result of Logical Not Operator: " + someRequirementsNotMet);









































